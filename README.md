# RockstarsTechReview

Vereisten/Requirements:
- Java 15 
- Google Chrome
- Intellij IDEA versie => 2020.2

#Automatische testen runnen

###API Testen
De API Testen te kunnen als volgt gestart worden:
- Draai alle testen, klik rechtermuisknop op src/APITests/java en dan vervolgens op 'Run Tests in java'. 
- Draai een testclas, klik rechtermuisknop op een van de testclasses in src/APITests/java/Tests en dan vervolgens op 'Run CLASSNAAM'.
- Draai één test, klik rechtermuisknop op de method of op de groene pijl bij de test in de class.

###UI Testen
De UI testen bevinden zich in src/test/java.
- Draai alle testen, klik rechtermuisknop op src/test/java en dan vervolgens op 'Run All Features in: test'. 
- Draai alle testen in een feature, klik rechtermuisknop op een van de testclasses in src/test/java/Features en dan vervolgens op 'Run FEATURE'.
- Draai één test, klik rechtermuisknop op een testscenario of op de groene pijl bij de scenario in een feature.
