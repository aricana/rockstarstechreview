package Tests;

import io.restassured.http.ContentType;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.put;

public class Products extends Config{


    @Test
    public void getAantalProducten()
    {
        String token = getToken();
        int aantalProducten = given().auth().oauth2(token).baseUri(baseUri).when().contentType(ContentType.JSON).get(productsUri).then().statusCode(200).and().extract().jsonPath().getList("$").size();
        Assertions.assertThat(aantalProducten).isEqualTo(21);

    }

    @Test
    public void wijzigEersteProduct()
    {
        String titel = "The Second Hoodie";
        String description = "The second, the hoodie, the gear...";
        int prijs = 129;
        String putRequestBody= "\"{\\n\" + \"  \\\"title\\\": \\\""+ titel + "\\\",\\n\" + \"  \\\"description\\\": \\\""+ description +"\\\",\\n\" + \"  \\\"price\\\":" +  prijs + ",\\n\" + \"  \\\"collectionType\\\": "+ 0 +",\\n\" + \"  \\\"productType\\\":"+  0 +" ,\\n\" + \"  \\\"image\\\": \\\" string \\\",\\n\" + \"  \\\"id\\\":"+  1+ "\\n\" + \"}\"";

        String token = getToken();
        String gewijzigdProductNaam = given().auth().oauth2(token).baseUri(baseUri).body(putRequestBody).when().contentType(ContentType.JSON).put(productsUri).then().statusCode(200).and().extract().jsonPath().get("title");
        Assertions.assertThat(gewijzigdProductNaam).isEqualTo(titel);

    }
    @Test
    public void toevoegenProduct()
    {
        String titel = "Ali's Hoodie";
        String description = "dit is een omschrijving";
        int prijs = 89;
        int collectionType = 1 ;
        int productType = 1;

        String token = getToken();
        int productId =
        given().
                auth().
                oauth2(token).
                baseUri(baseUri).
                contentType(ContentType.JSON).
                accept(ContentType.JSON).
                body("{\n" +
                "  \"title\": \""+ titel + "\",\n" +
                "  \"description\": \""+ description + "\",\n" +
                "  \"price\":"+ prijs + ",\n" +
                "  \"collectionType\":"+ collectionType + ",\n" +
                "  \"productType\":"+ productType + ",\n" +
                "  \"image\": \"string\"\n" +
                "}").
        when().
                post(productsUri).
        then().
                statusCode(200).and().extract().jsonPath().get("id");

        //verwijder aangemaakte product
        given().auth().oauth2(token).baseUri(baseUri).when().put(productsUri + "/" + productId).then().statusCode(200);

    }


}
