package Tests;

import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class Config {

    //Testvariabelen
    String gebruikersnaam = "ali@rockstars.nl";
    String wachtwoord = "Welkom01";
    String expectedVoorNaam = "ali";
    String expectedAchterNaam = "arican";
    String adminUsername = "i.am.it.rockstar@teamrockstars.nl";
    String adminPassword = "iamitrockstar!";

    //Omgevingsvariabelen
    String baseUri = "https://ali-trit-techscreening-testing-api.azurewebsites.net";
    String authenticateUri = "/Users/authenticate";
    String registrerenUri = "/Users/register";
    String meUri = "/Users/me";
    String cartUri= "/Cart";
    String cartClearUri= "/Cart/clear";
    String orderUri = "/Order";
    String productsUri = "/Products";
    String requestAuthentiseren = "{\n" + "  \"email\": \"" + gebruikersnaam + "\",\n" + "  \"password\": \"" + wachtwoord + "\"\n" + "}";
    String requestAdminAuthentiseren = "{\n" + "  \"email\": \"" + adminUsername + "\",\n" + "  \"password\": \"" + adminPassword + "\"\n" + "}";

    @Test
    public String getToken()
    {
        String token = given().baseUri(baseUri).when().contentType(ContentType.JSON).body(requestAdminAuthentiseren).post(authenticateUri).then().extract().jsonPath().get("token");
        return token;

    }

}
