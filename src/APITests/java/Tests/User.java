package Tests;

import io.restassured.http.ContentType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class User extends Config {

//    //Testvariabelen
//    String gebruikersnaam = "ali@rockstars.nl";
//    String wachtwoord = "Welkom01";
//    String expectedVoorNaam = "ali";
//    String expectedAchterNaam = "arican";
//
//    //Omgevingsvariabelen
//    String baseUri = "https://ali-trit-techscreening-testing-api.azurewebsites.net";
//    String authenticateUri = "/Users/authenticate";
//    String registrerenUri = "/Users/register";
//    String meUri = "/Users/me";
//    String requestAuthentiseren = "{\n" + "  \"email\": \"" + gebruikersnaam + "\",\n" + "  \"password\": \"" + wachtwoord + "\"\n" + "}";

//    @BeforeEach public String
//    logInReturnToken()
//    {
//        String token = given().baseUri(baseUri).when().contentType(ContentType.JSON).body(requestAuthentiseren).post(authenticateUri).then().extract().jsonPath().get("token");
//        return token;
//
//
//    }


    @Test
    public void authentiserenUser() {

        String voorNaam = given().baseUri(baseUri).when().contentType(ContentType.JSON).body(requestAuthentiseren).post(authenticateUri).then().extract().jsonPath().get("firstName");
        String achterNaam = given().baseUri(baseUri).when().contentType(ContentType.JSON).body(requestAuthentiseren).post(authenticateUri).then().extract().jsonPath().get("lastName");

        //Validatie
        Assertions.assertTrue(voorNaam.contentEquals(expectedVoorNaam));
        Assertions.assertTrue(achterNaam.contentEquals(expectedAchterNaam));

    }

    @Test
    public void RegistrerenUser() {
        String nieuweMailAdres = "test@rockstars.nl";
        String nieuweWachtwoord = "Welkom02";
        given().
                baseUri(baseUri).
        when().
                contentType(ContentType.JSON).
                body(
                        "{\n" +
                                "  \"firstName\": \"" + expectedVoorNaam + "\",\n" +
                                "  \"lastName\": \"" + expectedAchterNaam + "\",\n" +
                                "  \"email\": \"" + nieuweMailAdres + "\",\n" +
                                "  \"password\": \"" + nieuweWachtwoord + "\"\n" +
                                "}").
                post(registrerenUri).
        then().
                statusCode(200).and().
                extract().response().prettyPrint();


    }

    @Test
    public void getUser()
    {
        String token = getToken();
        given().
                auth().
                oauth2(token).
                baseUri(baseUri).
        when().
                contentType(ContentType.JSON).
                get(meUri).
        then().
                statusCode(200).and().
                extract().jsonPath().prettyPrint();


    }


}
