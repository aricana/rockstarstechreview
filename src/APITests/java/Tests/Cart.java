package Tests;

import io.restassured.http.ContentType;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class Cart extends Config{

    @Test
    public void postCart()
    {
        String productId = "1";
        String aantal = "2";
        String token = getToken();
        int aantalProducten = given().auth().oauth2(token).contentType(ContentType.JSON).baseUri(baseUri).body("{\n" + "  \"productId\":"+ productId+",\n" + "  \"quantity\":"+ aantal+"\n" + "}").when().post(cartUri).then().statusCode(200).and().extract().jsonPath().getList("$").size();
        Assertions.assertThat(aantalProducten).isEqualTo(1);

        //leegCart
        int cartIsLeeg = given().
                auth().oauth2(token).
                contentType(ContentType.JSON).
                baseUri(baseUri).
                body("{\n" + "  \"productId\":"+ productId+",\n" + "  \"quantity\":"+ aantal+"\n" + "}").
        when().
                delete(cartClearUri).
        then().
                statusCode(200).and().
                extract().jsonPath().getList("$").size();
        Assertions.assertThat(cartIsLeeg).isEqualTo(0);






    }
}
