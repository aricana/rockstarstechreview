package Tests;

import io.restassured.http.ContentType;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class Order extends Config{

    @Test
    public void getOrder()
    {
        String token = getToken();
        int aantalProducten=given().auth().oauth2(token).contentType(ContentType.JSON).baseUri(baseUri).when().get(orderUri).then().statusCode(200).and().extract().jsonPath().getList("products").size();
        Assertions.assertThat(aantalProducten).isEqualTo(0);


    }

    @Test
    public void postOrder()
    {

        String token = getToken();
        given().
                auth().oauth2(token).
                contentType(ContentType.JSON).
                baseUri(baseUri).
                body("{\n" +
                "  \"billingAddress\": {\n" +
                "    \"id\": 1 ,\n" +
                "    \"street\": \"tasmanialaan \",\n" +
                "    \"houseNumber\": 30,\n" +
                "    \"houseNumberAffix\": \"\",\n" +
                "    \"postalCode\": \"1012XP\",\n" +
                "    \"city\": \"Amsterdamn\"\n" + "  },\n" +
                "  \"shippingAddress\": {\n" +
                "    \"id\": 1,\n" +
                "    \"street\": \"tasmanialaan\",\n" +
                "    \"houseNumber\": 30,\n" +
                "    \"houseNumberAffix\": \"\",\n" +
                "    \"postalCode\": \"1012XP\",\n" +
                "    \"city\": \"Amsterdamn\"\n" +
                "  }\n" +
                "}").
        when().
                post(orderUri).
        then().
                statusCode(200).and().
                extract().response().prettyPrint();

    }
}
