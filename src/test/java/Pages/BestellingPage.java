package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BestellingPage {
    WebDriver driver;

    private By straatLocator = By.xpath("//input[@formcontrolname='street']");
    private By huisNummerLocator = By.xpath("//input[@formcontrolname='houseNumber']");
    private By postcodeLocator = By.xpath("//input[@formcontrolname='postalCode']");
    private By stadLocator = By.xpath("//input[@formcontrolname='city']");
    private By buttonLetsRock = By.cssSelector("button.mat-focus-indicator.mat-raised-button.mat-button-base.mat-accent.ng-star-inserted");

    public BestellingPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public void selecteerProduct(String product)
    {
        WebElement selectProduct = driver.findElement(By.xpath("//h3[contains(text(),'" + product + "')]/parent::*"));

        @Deprecated
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfAllElements(selectProduct));

        selectProduct.click();
    }

    public void vulInStraat(String straat)
    {
        @Deprecated
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(straatLocator));

        driver.findElement(straatLocator).sendKeys(straat);

    }
    public void vulInHuisnummer(String huisNummer)
    {
        @Deprecated
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(huisNummerLocator));

        driver.findElement(huisNummerLocator).sendKeys(huisNummer);

    }

    public void vulInPostcode(String postcode)
    {
        @Deprecated
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(postcodeLocator));

        driver.findElement(postcodeLocator).sendKeys(postcode);

    }

    public void vulInStad(String stad)
    {
        @Deprecated
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(stadLocator));

        driver.findElement(stadLocator).sendKeys(stad);

    }

    public void klikLetsRock()
    {
        @Deprecated
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(buttonLetsRock));

        driver.findElement(buttonLetsRock).click();


    }


}
