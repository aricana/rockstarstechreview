package Pages;

import groovyjarjarpicocli.CommandLine;
import org.assertj.core.api.Assert;
import org.assertj.core.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.w3c.dom.Text;

import java.sql.Time;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class HomePage {

    private WebDriver driver;
    ///Locators
    private By userProfileBtn = By.xpath("//mat-icon[text()[contains(., 'account_circle')]]");
    private By merchandiseTxt = By.xpath("//h2[contains (text(), 'Merchandise')]");
    private By collectiesBtn = By.xpath("(//div[contains (@class, 'mat-form-field')])[1]");
    private String collectieItem = "//span[contains (text(), 'item')]//parent::button";
    private By productNameTxt = By.xpath("//div[@class='product-image']//following-sibling::span[contains (text(), 'Essentials Collection')]");
    private By registreerBtn = By.xpath("//button[contains (text(), 'Registreer')]");
    private By loginBtn = By.xpath("//button[contains (text(), 'Login')]");
    private By cartBtn = By.xpath("//mat-icon[contains (text(), 'shopping_cart')]");

    public HomePage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public void clickGebruikersProfiel()
    {
        driver.findElement(userProfileBtn).click();
    }

    public void assertProductPage(){
        driver.findElement(merchandiseTxt).getText();
    }

    public void clickCollecties(){
        driver.findElement(collectiesBtn).click();
    }


    public void setCollectiesFilter(String newItem){
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//mat-pseudo-checkbox//following-sibling::span[contains (text(), '"+newItem+"')]")).click();
    }

    public void assertProductNaam(String product){
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        List<WebElement> producten = driver.findElements(By.xpath("//div[@class='product-image']//following-sibling::span[contains (text(), '"+product+"')]"));
        for (int i = 0; i< producten.size(); i++){

            @Deprecated
            WebDriverWait wait = new WebDriverWait(driver, 10);
            wait.until(ExpectedConditions.visibilityOfAllElements(producten));

            String enkelProduct = producten.get(i).getText();
            Assertions.assertThat(enkelProduct).isEqualTo(product);
        }
    }

    public void klikRegistreer(){
        driver.findElement(registreerBtn).click();
    }
    public void klikLogin(){driver.findElement(loginBtn).click();}
    public void klikCart(){driver.findElement(cartBtn).click();}
}
