package Pages;

import org.assertj.core.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class AccountPage {
    private WebDriver driver;

    private By registreerVoornaam = By.xpath("(//input)[1]");
    private By registreerAchternaam = By.xpath("(//input)[2]");
    private By registreerEmail = By.xpath("(//input)[3]");
    private By registreerWachtwoord = By.xpath("(//input)[4]");
    private By registreerBtn = By.xpath("//span[contains (text(), 'Registreer')]");
    private By registeerErrorTxt = By.xpath("//mat-error[contains (text(), 'is already taken')]");
    private By emailVeld = By.xpath("//input[@formcontrolname='email']");
    private By passwordVeld = By.xpath("//input[@formcontrolname='password']");
    private By loginBtn = By.xpath("//span[contains (text(), 'Login')]//parent::button");


    public AccountPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public void vulInRegistreerGegevens(String voornaam, String achternaam, String email, String wachtwoord){
        driver.findElement(registreerVoornaam).sendKeys(voornaam);
        driver.findElement(registreerAchternaam).sendKeys(achternaam);
        driver.findElement(registreerEmail).sendKeys(email);
        driver.findElement(registreerWachtwoord).sendKeys(wachtwoord);
    }
    public void klikRegistreer(){
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(registreerBtn).click();
    }
    public void assertFoutiefRegistratieMelding(){
        WebElement meldingTimer = driver.findElement(registeerErrorTxt);
        @Deprecated
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOf(meldingTimer));
        String foutiefMelding = driver.findElement(registeerErrorTxt).getText();
        Assertions.assertThat(foutiefMelding).isEqualTo("Email \"ali@arican.nl\" is already taken");
    }
    public void vulInEmail(String email){
        driver.findElement(emailVeld).sendKeys(email);
    }
    public void vulInWachtwoord(String wachtwoord){
        driver.findElement(passwordVeld).sendKeys(wachtwoord);
    }
    public void klikLogin(){
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(loginBtn).click();
    }

}
