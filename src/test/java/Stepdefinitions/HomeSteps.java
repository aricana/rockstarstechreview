package Stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;

public class HomeSteps extends BaseSteps{
    private WebDriver driver;

    public HomeSteps(){
        this.driver = DriverManager.driver;
    }

    @Then("De gebruiker heeft de producten gezien")
    public void deGebruikerHeeftDeProductenGezien(){
        homePage.assertProductPage();
    }

    @Given("De gebruiker heeft geklikt op Collecties")
    public void deGebruikerHeeftGekliktOpCollecties(){
        homePage.clickCollecties();
    }

    @Given("De gebruiker navigeert naar de inlogpagina")
    public void deGebruikerNavigeertNaarDeInlogPagina(){
        homePage.clickGebruikersProfiel();
        homePage.klikRegistreer();
    }

    @When("^de gebruiker selecteert een (.*)$")
    public void deGebruikerSelecteerEen(String collectie) {
        homePage.setCollectiesFilter(collectie);
    }

    @Then("^De gebruiker heeft de producten aan de hand van de filter gezien (.*)$")
    public void deGebruikerHeeftDeProductenAanDeHandVanDeFilterGezien(String collectie) {
        homePage.assertProductNaam(collectie);
    }


}
