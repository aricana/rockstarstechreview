package Stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;

import java.util.Arrays;
import java.util.List;

public class BestellingSteps extends BaseSteps {
    WebDriver driver;

    public BestellingSteps(){
        this.driver = DriverManager.driver;
    }
    @Given("De gebruiker is ingelogd (.*) (.*)$")
    public void deGebruikerIsIngelogd(String email, String wachtwoord){
        homePage.clickGebruikersProfiel();
        homePage.klikLogin();
        accountPage.vulInEmail(email);
        accountPage.vulInWachtwoord(wachtwoord);
        accountPage.klikLogin();

    }
    @When("^De gebruiker producten (.*) in zijn winkelmand plaatst$")
    public void deGebruikerProductenInZijnWinkelmandPlaatst(String producten){
    //producten achter elkaar noteren
        List<String> product= Arrays.asList(producten.split(","));

        for (int i = 0; i < product.size(); i++)
        {
            bestellingPage.selecteerProduct(product.get(i));
        }

    }
    @When("^De gebruiker plaatst de bestelling (.*) (.*) (.*) (.*)$")
    public void deGebruikerPlaatstDeBestelling(String straat, String huisnr, String postcode, String stad){
        homePage.klikCart();
        bestellingPage.klikLetsRock();
        bestellingPage.vulInStraat(straat);
        bestellingPage.vulInHuisnummer(huisnr);
        bestellingPage.vulInPostcode(postcode);
        bestellingPage.vulInStad(stad);
        bestellingPage.klikLetsRock();

    }
    @Then("De bestelling is geplaatst")
    public void deBestellingIsGeplaatst(){

    }
}
