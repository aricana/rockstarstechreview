package Stepdefinitions;

import Pages.AccountPage;
import Pages.BestellingPage;
import Pages.HomePage;
import org.openqa.selenium.WebDriver;

public class BaseSteps {

    private WebDriver driver;
    protected HomePage homePage;
    protected AccountPage accountPage;
    protected BestellingPage bestellingPage;


    public BaseSteps(){
        this.driver = DriverManager.driver;
        homePage = new HomePage(driver);
        accountPage = new AccountPage(driver);
        bestellingPage = new BestellingPage(driver);

    }

}
