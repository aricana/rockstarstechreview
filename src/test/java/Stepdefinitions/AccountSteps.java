package Stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class AccountSteps extends BaseSteps {
    WebDriver driver;
    public AccountSteps(){
        this.driver = DriverManager.driver;
    }

    @When("^De gebruiker zijn gegevens (.*) (.*) (.*) (.*) invult$")
    public void deGebruikerZijnGegevensInvuld(String voornaam, String achternaam, String email, String wachtwoord){
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        accountPage.vulInRegistreerGegevens(voornaam, achternaam, email, wachtwoord);
    }
    @When("De gebruiker heeft op registreer button geklikt")
    public void deGebruikerHeeftOpRegistreerButtonGeklikt(){
        accountPage.klikRegistreer();
    }
    @Then("Krijgt de gebruiker een foutmelding")
    public void krijgtDeGebruikerEenFoutmelding(){
        accountPage.assertFoutiefRegistratieMelding();
    }

}
