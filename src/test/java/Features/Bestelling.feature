Feature: Bestelling gerelateerde scenario's

  @Regressie
  Scenario Outline: De gebruiker kan een bestelling plaatsen
    Given De gebruiker is ingelogd <email> <wachtwoord>
    When De gebruiker producten <producten> in zijn winkelmand plaatst
    And De gebruiker plaatst de bestelling <straat> <huisnummer> <postcode> <stad>
    Then De bestelling is geplaatst

    Examples:
    |email         |wachtwoord   | producten                  | straat  | huisnummer| postcode | stad       |
    |ali@arican.nl |arican222    | The One Hoodie,Plonsstar   | bla bla | 12        | 1012XP   | Amsterdam  |