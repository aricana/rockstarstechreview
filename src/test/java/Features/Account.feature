Feature: Account gerelateerde testgevallen

  @Regressie
  Scenario Outline: De gebruiker kan registreren
    Given De gebruiker navigeert naar de inlogpagina
    When De gebruiker zijn gegevens <voornaam> <achternaam> <email> <wachtwoord> invult
    And De gebruiker heeft op registreer button geklikt
    Then Krijgt de gebruiker een foutmelding

    Examples:
    |voornaam|achternaam|email|wachtwoord|
    |Ali     |Arican    |ali@arican.nl|arican222|

