Feature: Producten gerelateerde testgevallen

  @Regressie
  Scenario: De gebruiker is op de homepagina en kan de producten zien
    Then De gebruiker heeft de producten gezien

  @Regressie
  Scenario Outline: De gebruiker kan gebruikmaken van de filters
    Given De gebruiker heeft geklikt op Collecties
    When de gebruiker selecteert een <collectie>
    Then De gebruiker heeft de producten aan de hand van de filter gezien <collectie>


    Examples:
    | collectie             |
    | Essentials Collection |
    | Stardust Collection   |

